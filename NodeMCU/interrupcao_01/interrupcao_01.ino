#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h> 
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>

//Variaveis para conectar no wifi
const char* ssid = "neneh"; //VARIÁVEL QUE ARMAZENA O NOME DA REDE SEM FIO EM QUE VAI CONECTAR
const char* password = "Limaozinho"; //VARIÁVEL QUE ARMAZENA A SENHA DA REDE SEM FIO EM QUE VAI CONECTAR

const char* host = "door-monitoring.herokuapp.com";
const char* linkPost = "/api/measurements";
const int httpsPort = 443;
//SHA1 finger print of certificate use web browser to view and copy
const char fingerprint[] PROGMEM = "08 3B 71 72 02 43 6E CA ED 42 86 93 BA 7E DF 81 C4 BC 62 30";


#define LED D0    // pino led nodemcu
uint8_t interrupt01 = D2;
void ICACHE_RAM_ATTR IntCallback();
bool sensor01 = false;

unsigned long previousMillis = millis();
long interval = 12000;

void setup() {
  Serial.begin(9600);
  pinMode(LED, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(interrupt01), IntCallback, CHANGE);

  delay(1000);
  Serial.begin(115200);
  WiFi.mode(WIFI_OFF);        //Prevents reconnection issue (taking too long to connect)
  delay(1000);
  WiFi.mode(WIFI_STA);        //Only Station No AP, This line hides the viewing of ESP as wifi hotspot
  
  WiFi.begin(ssid, password);     //Connect to your WiFi router
  Serial.println("");
 
  Serial.print("Connecting");
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
 
  //If connection successful show IP address in serial monitor
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());  //IP address assigned to your ESP
}

void loop() {
  if (millis() - previousMillis > interval)
  {
    previousMillis = millis();
     
    if (notificarServidorHttpsPost())
    {
      limpaMemoria();
    }
  }
}

void ICACHE_RAM_ATTR IntCallback() {
  digitalWrite(LED, HIGH);
  print("\n--------------------------------------------\nSensor acionado");
  sensor01 = true;
}

bool notificarServidorHttpsPost()
{
  DynamicJsonDocument doc(2048);
  doc["mac"] = "a1111";
    if(sensor01)
    {
      doc["opened"] = "1";
    }
    else
    {
      doc["opened"] = "0";
    }
  
  // Serialize JSON document
  String json;
  serializeJson(doc, json);

  
//Post
  WiFiClientSecure httpsClient;    //Declare object of class WiFiClient
 
  Serial.println(host);
 
  Serial.printf("Using fingerprint '%s'\n", fingerprint);
  httpsClient.setFingerprint(fingerprint);
  httpsClient.setTimeout(15000); // 15 Seconds
  delay(1000);
  
  Serial.print("HTTPS Connecting");
  int r=0; //retry counter
  while((!httpsClient.connect(host, httpsPort)) && (r < 30)){
      delay(100);
      Serial.print(".");
      r++;
  }
  if(r==30) {
    Serial.println("Connection failed");
  }
  else {
    Serial.println("Connected to web");
  }
 
  Serial.print("requesting URL: ");
  Serial.println(host);
  /*
   POST /post HTTP/1.1
   Host: postman-echo.com
   Content-Type: application/x-www-form-urlencoded
   Content-Length: 13
  
   say=Hi&to=Mom
    
   */
  Serial.println(json);
  Serial.println(json.length());
 
  httpsClient.print(String("POST ") + linkPost + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               
               /*"Content-Type: application/json"+ "\r\n" +
               "Content-Length: " + 50 + "\r\n\r\n" +
                json);*/
               
               "Content-Type: application/x-www-form-urlencoded"+ "\r\n" +
               "Content-Length: 18" + "\r\n\r\n" +
               "mac=a1111&opened=1" + "\r\n" +
               "Connection: close\r\n\r\n");
 
  Serial.println("request sent");
                  
  while (httpsClient.connected()) {
    String line = httpsClient.readStringUntil('\n');
    if (line == "\r") {
      Serial.println("headers received");
      break;
    }
  }
 
  Serial.println("reply was:");
  Serial.println("==========");
  String line;
  while(httpsClient.available()){        
    line = httpsClient.readStringUntil('\n');  //Read Line by Line
    Serial.println(line); //Print response
  }
  Serial.println("==========");
  Serial.println("closing connection");               
}

bool conectWifi()
{
  print("\n\nConectando a ");
  print(ssid); //ESCREVE O NOME DA REDE NA SERIAL

  WiFi.begin(ssid, password); //PASSA OS PARÂMETROS PARA A FUNÇÃO QUE VAI FAZER A CONEXÃO COM A REDE SEM FIO
  while (WiFi.status() != WL_CONNECTED) { //ENQUANTO STATUS FOR DIFERENTE DE CONECTADO
    delay(500);
    print(".");
  }

  print("\nConectado a rede sem fio ");
  print(ssid); //ESCREVE O NOME DA REDE NA SERIAL,
}

void limpaMemoria() {
  print("\nLimpando memoria sendores..");
  digitalWrite(LED, LOW);
  bool sensor01 = false;
  print("\nMemoria sendores limpa!\n");
}

void print(String texto)
{
  Serial.print(texto);
}
