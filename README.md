# Node MCU Router

Projeto para inserção dos códigos desenvolvidos para a placa ->**Node MCU** (Sistemas Embarcados 2).

## Arduino IDE

https://www.arduino.cc/en/Main/Software

# Workflow

### Wifi-Notify v1.0

1. Abra o Arduino IDE após a instalação
2. Vá em **Arquivo > Preferencias** (*File > Preference*)
3. Na parte de "*URLs adicionais para Gerenciadores de Placas*" adicione: http://arduino.esp8266.com/stable/package_esp8266com_index.json
4. Em seguida em **Ferramentas > Placas > Gerenciador de Placas**...
5. Procure pelo pacote: **esp8266** by **esp8266 community** e instale
6. Pronto, só programar =)

### Captive Hotspot v4.0

1. Downloading files:
    1. Download Arduino IDE 1.8.9 (portable):
https://downloads.arduino.cc/arduino-1.8.9-windows.zip
    2. Download "lwip_nat_arduino-master.zip":
https://codeload.github.com/martin-ger/lwip_nat_arduino/zip/master
    3. Download "hh2.golden.exe" (402 bytes):
https://github.com/pts/pts-tinype/raw/master/hh2.golden.exe

2. Configure Arduino with ESP8266 library:
    1. Unzip "arduino-1.8.9-windows.zip" to "C:\App\arduino-ide\"
    2. Makedir "portable" in "C:\App\arduino-ide\"
    3. Create new file : "C:\App\arduino-ide\portable\preferences.txt"
    4. Add line with : 
    boardsmanager.additional.urls=http://arduino.esp8266.com/stable/package_esp8266com_index.json
    5. Open IDE Arduino and install ESP8266 library in menu :
    Tools / Boards: "Arduino..." / Boards Manager

3. Install the lwip_nat_arduino library:
    1. Copy and rename "hh2.golden.exe" to "C:\App\arduino-ide\make.exe"
    (to have an empty "make" utility)
    2. Open "lwip_nat_arduino-master.zip"
    3. Next operation in folder :
    "C:\App\arduino-ide\portable\packages\esp8266\hardware\esp8266\2.5.2\tools\"
    4. Rename lwip\ with lwip.orig\ in sdk\
    5. Unzip  lwip\                 in sdk\lwip\
    6. Unzip  liblwip_src.a         in sdk\lib\

4. Change the compilation mode :
    1. Menu : Tools / lwIP Variant: "v1.4 Compile from source"
    2. Menu : Tools / Erase Flash: "Sketch + WiFi Settings"
    
#### References

- https://github.com/martin-ger/lwip_nat_arduino
- https://github.com/martin-ger/lwip_nat_arduino/issues/1

### Escape String

- https://tomeko.net/online_tools/cpp_text_escape.php?lang=en


## NodeMCU DEVKIT 1.0 Specification:

* **Developer** : ESP8266 Opensource Community
* **Type** :  Single-board microcontroller
* **Operating system** : XTOS
* **CPU** : ESP8266
* **Memory** : 128kBytes
* **Storage** : 4MBytes
* **Power By** : USB
* **Power Voltage** : 3v ,5v (used with 3.3v Regulator which inbuilt on Board using Pin VIN)
* **Code** : Arduino Cpp
* **IDE Used** : Arduino IDE
* **GPIO** : 10

## Código de Exemplo

```cpp 
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

// Replace with your network credentials
const char* ssid = "WifiNotification";
const char* password = "12345678";

ESP8266WebServer server(80);   //instantiate server at port 80 (http port)

String page = "";
int LEDPin = 13;
void setup(void){
  //the HTML of the web page
  page = "<h1>Simple NodeMCU Web Server</h1><p><a href=\"LEDOn\"><button>ON</button></a>&nbsp;<a href=\"LEDOff\"><button>OFF</button></a></p>";
  //make the LED pin output and initially turned off
  pinMode(LEDPin, OUTPUT);
  digitalWrite(LEDPin, LOW);
   
  delay(1000);
  Serial.begin(115200);
  WiFi.softAP(ssid, password); //begin WiFi access point

//  Wait for connection
//  while (WiFi.status() != WL_CONNECTED) {
//    delay(500);
//    Serial.print(".");
//  }
  
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.softAPIP()); 
  Serial.println(WiFi.softAPmacAddress());
   
  server.on("/", [](){
    server.send(200, "text/html", page);
    Serial.println("Endpoint -> /");
  });
  server.on("/LEDOn", [](){
    server.send(200, "text/html", page);
    digitalWrite(LEDPin, HIGH);
    Serial.println("LEDOn");
    delay(1000);
  });
  server.on("/LEDOff", [](){
    server.send(200, "text/html", page);
    digitalWrite(LEDPin, LOW);
    Serial.println("LEDOff");
    delay(1000); 
  });
  server.begin();
  Serial.println("Web server started!");
}
 
void loop(void){
  server.handleClient();
}
 
```